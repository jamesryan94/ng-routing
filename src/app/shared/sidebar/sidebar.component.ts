import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MotorService } from 'src/app/services/motor/motor.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy{

  motors: any[] = [];
  subscriptions = new Subscription();

  constructor(
    private motorService: MotorService,
    private router: Router) { }

  ngOnInit(): void {
    this.subscriptions.add(this.motorService.list().subscribe(motors => this.motors = motors));
  }

  selectMotor(id: string) {
    this.motorService.updateSelection(id, 'motor');
    this.router.navigate(['motors', id]);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}

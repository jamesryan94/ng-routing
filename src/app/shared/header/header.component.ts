import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { MotorService } from 'src/app/services/motor/motor.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  topLevelPath: string | undefined;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private motorService: MotorService) {
      this.topLevelPath = this.route?.firstChild?.snapshot?.url[0].path ?? 'motors';
    }

  ngOnInit(): void {
    this.router.events.subscribe(event =>{
      if (event instanceof NavigationEnd) {
        this.topLevelPath = this.route.firstChild?.snapshot.url[0].path;
      }
    });
  }

  resetSelection() {
    this.motorService.resetSelection();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/signin'])
  }

}

export type SelectionOptions = 'motor'|'location';

export class MotorSelection {
  id: string;
  selectionType: SelectionOptions;
}



export class Motor{
  name: string;
  status: string;
  id: string;
  location: string;
  data: {
    x: number,
    y: number
  }[];
  components: {
    name: string,
    status: string
  }[];

  constructor(motor: Partial<Motor>) {
    Object.assign(this, motor);
  }
}

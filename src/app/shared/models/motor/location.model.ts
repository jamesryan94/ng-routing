import { MotorSelection } from "./motor-selection.model";
import { Motor } from "./motor.model";

export class Location {
  id: string;
  name: string;
  motors: Motor[];
}

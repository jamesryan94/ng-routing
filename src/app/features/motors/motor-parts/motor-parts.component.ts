import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MotorService } from 'src/app/services/motor/motor.service';
import { Motor } from 'src/app/shared/models/motor/motor.model';

@Component({
  selector: 'app-motor-parts',
  templateUrl: './motor-parts.component.html',
  styleUrls: ['./motor-parts.component.scss']
})
export class MotorPartsComponent implements OnInit {

  motor: Motor;

  constructor(
    private route: ActivatedRoute,
    private motorService: MotorService
  ) { }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap(params => this.motorService.getById(params.get('id'))))
      .subscribe(motor => this.motor = motor);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotorPartsComponent } from './motor-parts.component';

describe('MotorPartsComponent', () => {
  let component: MotorPartsComponent;
  let fixture: ComponentFixture<MotorPartsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotorPartsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotorPartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

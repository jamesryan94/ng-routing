import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { delay } from 'rxjs/operators';
import { MotorService } from 'src/app/services/motor/motor.service';
import { MotorSelection } from 'src/app/shared/models/motor/motor-selection.model';
import { Motor } from 'src/app/shared/models/motor/motor.model';

@Component({
  selector: 'app-motor-overview',
  templateUrl: './motor-overview.component.html',
  styleUrls: ['./motor-overview.component.scss']
})
export class MotorOverviewComponent implements OnInit, OnDestroy {

  loading = true;
  selected: MotorSelection;
  subscriptions = new Subscription();
  motor: Motor;
  motorList: Motor[];

  constructor(
    private motorService: MotorService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        console.log('Updated url')
        console.log(this.route.snapshot.children[0])
      }
    })
    const routedId = this.route.snapshot.children[0]?.paramMap.get('id');
    if (routedId) {
      this.motorService.updateSelection(routedId, 'motor');
    }
    this.subscriptions.add(this.motorService.list().pipe(delay(500)).subscribe(m => this.motorList = m));
    this.subscriptions.add(this.motorService.selection$.subscribe(s => {
      this.selected = s;
      if (this.selected?.selectionType === 'motor') {
        this.motorService.getById(s.id).pipe(delay(500)).subscribe(m => this.motor = m);
      }
    }));
    this.router.events.subscribe(event => event)
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotorsRoutingModule } from './motors-routing.module';
import { MotorOverviewComponent } from './motor-overview/motor-overview.component';
import { MotorPartsComponent } from './motor-parts/motor-parts.component';
import { MotorPlotsComponent } from './motor-plots/motor-plots.component';
import { MotorDetailsComponent } from './motor-details/motor-details.component';


@NgModule({
  declarations: [
    MotorOverviewComponent,
    MotorPartsComponent,
    MotorPlotsComponent,
    MotorDetailsComponent
  ],
  imports: [
    CommonModule,
    MotorsRoutingModule
  ]
})
export class MotorsModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MotorPlotsComponent } from './motor-plots.component';

describe('MotorPlotsComponent', () => {
  let component: MotorPlotsComponent;
  let fixture: ComponentFixture<MotorPlotsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MotorPlotsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MotorPlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

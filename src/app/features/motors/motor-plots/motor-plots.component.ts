import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { MotorService } from 'src/app/services/motor/motor.service';
import { Motor } from 'src/app/shared/models/motor/motor.model';

@Component({
  selector: 'app-motor-plots',
  templateUrl: './motor-plots.component.html',
  styleUrls: ['./motor-plots.component.scss']
})
export class MotorPlotsComponent implements OnInit {
  motor: Motor;

  constructor(
    private route: ActivatedRoute,
    private motorService: MotorService) { }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap(params => this.motorService.getById(params.get('id'))))
      .subscribe(motor => this.motor = motor);
  }

}

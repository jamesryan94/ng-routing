import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MotorPartsComponent } from './motor-parts/motor-parts.component';
import { MotorOverviewComponent } from './motor-overview/motor-overview.component';
import { MotorPlotsComponent } from './motor-plots/motor-plots.component';
import { MotorDetailsComponent } from './motor-details/motor-details.component';

const routes: Routes = [
  {path: '', component: MotorOverviewComponent,
  children: [
    {path: ':id/status', component: MotorPartsComponent},
    {path: ':id/details', component: MotorDetailsComponent},
    {path: ':id/data', component: MotorPlotsComponent},
    {path: ':id', pathMatch: 'full', redirectTo: ':id/details'}
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotorsRoutingModule { }

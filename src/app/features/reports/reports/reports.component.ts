import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

  reports = [
    {name: 'Monday Report', id:0, url:'2021-10-14', description: 'Weekly Summary'},
    {name: 'HVAC Report', id:1, url:'2021-10-12', description: 'Report on HVAC Motors'},
    {name: 'Daily Report', id:2, url:'2021-10-14', description: 'Daily snapshot of motors'},
  ];

  constructor() { }

  ngOnInit(): void {
  }

}

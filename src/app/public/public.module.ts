import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';



@NgModule({
  declarations: [
    SigninComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule
  ]
})
export class PublicModule { }

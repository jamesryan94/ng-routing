import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { MotorSelection, SelectionOptions } from 'src/app/shared/models/motor/motor-selection.model';
import { Motor } from 'src/app/shared/models/motor/motor.model';

@Injectable({
  providedIn: 'root'
})
export class MotorService {

  motors = [
    {name: 'Factory HVAC', status: 'good', id: 'M0', location: 'Limerick', data: [{x:0,y:0}], components: [{name:'Bearing', status:'Good'}]},
    {name: 'Office HVAC', status: 'good', id: 'M1', location: 'Limerick', data: [{x:0,y:0}, {x:1,y:0},{x:2,y:2}], components: [{name:'Bearing', status:'Good'}]},
    {name: 'Extrusion Motor', status: 'bad', id: 'M2', location: 'Boston', data: [{x:0,y:0}, {x:1,y:0}, {x:2,y:3}], components: [{name:'Performance', status:'Bad'}]},
    {name: 'Belt Driver', status: 'degraded', id: 'M3', location: 'Boston', data: [{x:0,y:0}, {x:1,y:1}, {x:2,y:3}], components: [{name:'Rotor', status:'Degraded'}]},
    {name: 'Hopper 1', status: 'degraded', id: 'M4', location: 'Cork', data: [{x:0,y:0}, {x:2,y:3}], components: [{name:'Stator', status:'Degraded'}]},
  ]

  selection$ = new BehaviorSubject<MotorSelection>(null);

  constructor() { }

  list(): Observable<Array<Motor>> {
    return of(this.motors.map(m => new Motor(m))).pipe(delay(500));
  }

  getById(id: string): Observable<Motor> {
    return of(this.motors.find(m => m.id === id)).pipe(delay(200));
  }

  updateSelection(id:string, selectionType: SelectionOptions) {
    this.selection$.next({id, selectionType})
  }

  resetSelection() {
    this.selection$.next(null);
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './public/page-not-found/page-not-found.component';
import { SigninComponent } from './public/signin/signin.component';
import { AuthGuard } from './services/auth/auth.guard';
import { PageComponent } from './shared/page/page.component';

const routes: Routes = [
  {path: 'signin', component: SigninComponent},
  {path: '', component: PageComponent ,children: [
    {
      path: 'motors',
      loadChildren: () => import('./features/motors/motors.module').then(m => m.MotorsModule),
      // canActivate: [AuthGuard]
    },
    {
      path: 'reports',
      loadChildren: () => import('./features/reports/reports.module').then(m => m.ReportsModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'alerts',
      loadChildren: () => import('./features/alerts/alerts.module').then(m => m.AlertsModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'admin',
      loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule),
      canActivate: [AuthGuard]
    },
    { path: '', pathMatch: 'full', redirectTo: 'motors'}
  ]
  },
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
